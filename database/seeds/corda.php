<?php

use Illuminate\Database\Seeder;

class corda extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d h:i:s');
        //ID 1
        DB::table('corda')->insert(
            array(
                'corda' => 'Amarello',
                'nivel' => 'Avanzado',
                'created_at' => $now,
                'updated_at' => $now,
            )
        );
    }
}
