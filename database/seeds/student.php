<?php

use Illuminate\Database\Seeder;

class student extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d h:i:s');
        //ID 1
        DB::table('student')->insert(
            array(
                'name' => 'Bruce Carlos',
                'apepat' => 'Rodriguez',
                'apemat' => 'Ramos',
                'email' => 'bruce.rodriguez94@gmail.com',
                'apodo' => 'Linterna',
                'id_corda' => '1',
                'id_centro' => '1',
                'movil1' => '931061442',
                'id_distrito' => 'Surco',
                'direccion' => 'Av. Santa Isabel de Villa Mz l lote 1',
                'fecnac' => '1994-03-26',
                'sexo' => '1',
                'fecini' => '2014-05-03',
                'dni' => '72567038',
                'id_entrenador' => '1',
                'status' => '1',
                'created_at' => $now,
                'updated_at' => $now,
            )
        );
    }
}
