<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->default('NULL');
            $table->string('apepat')->nullable()->default('NULL');
            $table->string('apemat')->nullable()->default('NULL');
            $table->string('email')->nullable()->default('NULL');
            $table->string('apodo')->nullable()->default('NULL');
            $table->string('id_rol')->nullable()->default('NULL');
            $table->string('id_corda')->nullable()->default('NULL');
            $table->string('id_centro')->nullable()->default('NULL');
            $table->string('fijo1')->nullable()->default('NULL');
            $table->string('fijo2')->nullable()->default('NULL');
            $table->string('movil1')->nullable()->default('NULL');
            $table->string('movil2')->nullable()->default('NULL');
            $table->string('id_distrito')->nullable()->default('NULL');
            $table->string('direccion')->nullable()->default('NULL');
            $table->string('fecnac')->nullable()->default('NULL');
            $table->string('sexo')->nullable()->default('NULL');
            $table->string('fecini')->nullable()->default('NULL');
            $table->string('dni')->nullable()->default('NULL');
            $table->string('id_profesion')->nullable()->default('NULL');
            $table->string('id_entrenador')->nullable()->default('NULL');
            $table->string('status')->nullable()->default('NULL');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
