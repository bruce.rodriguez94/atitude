<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Alumno
Route::get('/student', function () {
    return view('student.content ');
});
Route::post('/createStudent', 'StudentController@createStudent');
Route::get('/readStudent', 'StudentController@readStudent');
Route::post('/updateStudent', 'StudentController@updateStudent');
Route::post('/deleteStudent/{id}', 'StudentController@deleteStudent');
Route::post('/setValues/{id}', 'StudentController@setValues');