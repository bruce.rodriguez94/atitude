<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "student";
    public $timestamps = false;

    public static function getStudent(){
        return Student::join('corda', 'student.id_corda', '=', 'corda.id')
        ->where('status',1)->get();
    }
}
