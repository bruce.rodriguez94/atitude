<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
class StudentController extends Controller
{
  public function createStudent(Request $request){
    $now = date('Y-m-d h:i:s');
    $data = new Student();
    $data->name = $request->name;
    $data->id_corda = 1;
    $data->status = 1;
    $data->created_at = $now;
    $data->updated_at = $now;
    $data->save();
  }
  public function readStudent() {
		$data = Student::getStudent();
		return $data;
  }
  public function updateStudent(Request $request){
    return $request;
		$data = Student::where('id', 1)->first();
    $data->name = $request->name;
		$data->save();
	}
  public function deleteStudent($id) {
    $data = Student::where('id', $id)->first();
    $data->status = 0;
    $data->save();
  }
  public function setValues($id){
    return Student::where('id', $id)->first();
  }
}
