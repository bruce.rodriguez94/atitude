<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
        		</button>
      		</div>
      		<div class="modal-body">
	  			<table class="table table-striped">
					<tr>
						<td>Nombre :</td>
						<td><input type="text" v-model="newItem.name"></td>
					</tr>
					<tr>
						<td>Apellido Paterno:</td>
						<td><input id="txtape_pat" type="text"></td>
					</tr>
					<tr>
						<td>Apellido Matermo:</td>
						<td><input id="txtape_mat" type="text"></td>
					</tr>
					<tr>
						<td>Corda :</td>
						<td>
		<select id="txtcorda" class="custom-select">
			<option value="Cinza-Branco">Cinza-Branco</option>
			<option value="Cinza-Branco Verde Pastel">Cinza-Branco Verde Pastel</option>
			<option value="Cinza-Branco Amarelo Pastel">Cinza-Branco Amarelo Pastel</option>
			<option value="Cinza-Branco Azul Pastel">Cinza-Branco Azul Pastel</option>
			<option value="Cinza">Cinza</option>
			<option value="Cinza-Verde">Cinza-Verde</option>
			<option value="Cinza-Amarelo">Cinza-Amarelo</option>
			<option value="Cinza-Azul">Cinza-Azul</option>
			<option value="Cinza-Verde Amarelo">Cinza-Verde-Amarelo</option>
			<option value="Cinza-Verde Azul">Cinza-Verde Azul</option>
			<option value="Cinza-Amarelo Azul">Cinza-Amarelo Azul</option>
			<option value="Cinza-Verde Amarelo-Azul">Cinza-Verde Amarelo-Azul</option>
			<option value="Verde">Verde</option>
			<option value="Amarelo">Amarelo</option>
			<option value="Azul">Azul</option>
			<option value="Verde-Amarelo">Verde-Amarelo</option>
			<option value="Verde-Azul">Verde-Azul</option>
			<option value="Amarelo-Azul">Amarelo-Azul</option>
			<option value="Verde-Amarelo-Azul">Verde-Amarelo-Azul</option>
			<option value="Verde-Branco">Verde-Branco</option>
			<option value="Amarelo-Branco">Amarelo-Branco</option>
			<option value="Azul-Branco">Azul-Branco</option>
			<option value="Vermelho">Vermelho</option>
			<option value="Branco">Branco</option>
			<option value="Verde-Amarelo-Azul-Branco">Verde-Amarelo-Azul-Branco</option>
		</select>
		</td>
	</tr>
	<tr>
		<td>Nivel :</td>
		<td>
		<select id="txtnivel" class="custom-select">
			<option value="Baby Infantil">Baby Infantil</option>
			<option value="Infantil">Infantil</option>
			<option value="Infantil Juvenil">Infantil Juvenil</option>
			<option value="Juvenil Adulto">Juvenil Adulto</option>
			<option value="Intermediario">Intermediario</option>
			<option value="Avanzado">Avanzado</option>
			<option value="Graduado">Graduado</option>
			<option value="Formado">Formado</option>
			<option value="Instructor">Instructor</option>
			<option value="Profesor">Profesor</option>
			<option value="Contra Mestre">Contra Mestre</option>
			<option value="Mestre">Mestre</option>
			<option value="Mestre Superior">Mestre Superior</option>
			<option value="Estagiario">Estagiario</option>
		</select>
		</td>
	</tr>
	<tr>
		<td>Local :</td>
		<td>	
		<select id="txtlocal" class="custom-select">
			<option value="San Juan de Lurigancho">San Juan de Lurigancho</option>
			<option value="San Miguel">San Miguel</option>
			<option value="Rimac">Rimac</option>
			<option value="San Isidro">San Isidro</option>
			<option value="Callao">Callao</option>
			<option value="Pueblo Libre">Pueblo Libre</option>
			<option value="Los Olivos">Los Olivos</option>
			<option value="Universidad Nacional de Ingenieria">Universidad Nacional de Ingenieria</option>
			<option value="Universidad Nacional del Callao">Universidad Nacional del Callao</option>
			<option value="Universidad Catolica Sedes Sapientiae">Universidad Catolica Sedes Sapientiae</option>
			<option value="Universidad Cesar Vallejo">Universidad Cesar Vallejo</option>

		</select>
		</td>
	</tr>
	<tr>
		<td>Telefono 1 :</td>
		<td><input id="txttelefono1" type="text"></td>
	</tr>
	<tr>
		<td>Telefono 2 :</td>
		<td><input id="txttelefono2" type="text"></td>
	</tr>
	<tr>		
		<td>Celular 1</td>
		<td><input id="txtcelular1" type="text"></td>
	</tr>
	<tr>
		<td>Celular 2</td>
		<td><input id="txtcelular2" type="text"></td>
	</tr>
	<tr>
		<td>Direccion</td>
		<td><input id="txtdireccion" type="text"></td>
	</tr>
	<tr>
		<td>Distrito</td>
		<td><input id="txtdistrito" type="text"></td>
	</tr>
	<tr>
		<td>Fecha de Nacimiento</td>
		<td><input id="txtfec_nac" type="date"></td>
	</tr>
	<tr>
		<td>Sexo</td>
		<td>
		<select id="txtsexo" class="custom-select">
			<option value="Masculino">Masculino</option>
			<option value="Femenino">Femenino</option>
		</select>
		</td>
	</tr>
	<tr>
		<td>Fecha de Inicio</td>
		<td><input id="txtfec_ini" type="date"></td>
	</tr>
	<tr>
		<td>DNI</td>
		<td><input id="txtdni" type="text"></td>
	</tr>
	<tr>
		<td>Profesion</td>
		<td><input id="txtprofesion" type="text"></td>
	</tr>
	<tr>
		<td>Correo</td>
		<td><input id="txtcorreo" type="text"></td>
	</tr>
	<tr>
		<td>Estado</td>
		<td><input id="txtestado" type="text" value="Activo" disabled=""></td>
	</tr>
	<tr>
		<td>Entrenador</td>
		<td>
		<select id="txtentrenador" class="custom-select">
			<option value="Profesor Vampi">Profesor Vampi</option>
			<option value="Instructora Bitoca">Instructora Bitoca</option>
			<option value="Formada Liliana">Formada Liliana</option>
			<option value="Formado Vermelho">Formado Vermelho</option>
			<option value="Formada Fiorella">Formada Fiorella</option>
			<option value="Formado Boneco">Formado Boneco</option>
			<option value="Formada Lala">Formada Lala</option>
			<option value="Formado Roney">Formado Roney</option>
			<option value="Graduado Martin">Graduado Martin</option>
		</select>
	</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<button class="btn btn-primary" @click.prevent="createItem()">
				<span class="glyphicon glyphicon-plus"></span> ADD
			</button>
		</td>
	</tr>
	</div id="clear">
	</table>
	</form>
	<div id="clear"></div>
	</div>
	<div id="clear"></div>
	</div>
      </div>
    </div>
  </div>
</div>
