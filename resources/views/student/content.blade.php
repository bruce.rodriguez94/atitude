@extends('layouts.layout')
@section('title', 'Alumnos')
@section('header', 'Listado de Alumnos')
@section('content')
<div id="app">
    <table-student></table-student>
</div>
<script type="text/javascript" src="/js/app.js"></script>
@endsection
