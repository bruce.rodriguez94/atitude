require('./bootstrap');

window.Vue = require('vue');

Vue.component('table-student', require('./components/student/ContentComponent.vue').default);

axios.defaults.headers = { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
const app = new Vue({ 
    el: '#app',
});
